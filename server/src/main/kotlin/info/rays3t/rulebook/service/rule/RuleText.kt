package info.rays3t.rulebook.service.rule

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "rule_rule_text")
data class RuleText(
        @Id
        @Column(name = "rule_text_id")
        var id: Int,

        @ManyToOne
        @JoinColumn(name = "rule_rule_id")
        @JsonIgnore
        var rule: Rule,

        @ManyToOne
        @JoinColumn(name = "rule_text_Language_id")
        @JsonIgnore
        var language: Language,

        @Column(name = "translation")
        var translation: String

)