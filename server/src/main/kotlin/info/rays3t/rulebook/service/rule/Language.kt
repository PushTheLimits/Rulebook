package info.rays3t.rulebook.service.rule

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "language")
data class Language(
        @Id
        @Column(name = "language_code")
        var languageCode: String,

        @Column(name = "language_name_international")
        var nameInternational: String,

        @Column(name = "language_name_national")
        var nameNational: String,

        @Column(name = "language_icon")
        var icon: String
)