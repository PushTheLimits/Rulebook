package info.rays3t.rulebook

import info.rays3t.rulebook.service.rule.DomainRepository
import info.rays3t.rulebook.service.rule.RuleDomain
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class Test {

    @Autowired
    lateinit var domainRepo: DomainRepository

    @RequestMapping("/test")
    fun test(): RuleDomain? {
        var rules = domainRepo.findAll()
        return rules.first()
    }
}