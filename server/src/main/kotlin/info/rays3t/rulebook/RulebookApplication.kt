package info.rays3t.rulebook

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication

@SpringBootApplication
@EntityScan
class RulebookApplication

fun main(args: Array<String>) {
    runApplication<RulebookApplication>(*args)
}
